const todoItems = [];
todoItems.push({
    title: 'Buy milk',
    done: true
});
todoItems.push({
    title: 'Learn about filter in JS',
    done: false
});
todoItems.push({
    title: 'Complete task 15',
    done: false
});
todoItems.push({
    title: 'Buy a unicorn',
    done: true
});

// Filter always returns an array. If NO items match the condition, it returns and empty array [].

// Filter out all the items that are done = true;
const completedItems = todoItems.filter((item)=>{
    return item.done == true;
});
console.log('Completed items: ', JSON.stringify(completedItems));

const incompleteItems = todoItems.filter((item)=>{
    return item.done == false;
});
console.log('Incomplete items: ', JSON.stringify(incompleteItems));