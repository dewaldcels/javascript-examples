// If - Else
let isCool = true;
if (isCool) {
    console.log('Wow! I am really cool! 😎');
} else {
    console.log('Damn, I am not cool after all... 💀');
}

// If - Else If - Else
let yourMark = 30;
let passMark = 50;
let passWithHonours = 60;

if (yourMark >= passWithHonours) {
    console.log('Passed test with honours! 👩‍🎓');
} else if (yourMark >= passMark && yourMark < passWithHonours) {
    console.log('Passed the test! 🤓');
} else {
    console.log('Oh no... You failed the test... 💀');
}

// SWITCH
// Switch is good for comparing simple values
// Not good for complicated conditions
const mood = 'Good';

switch (mood) {
    case 'Bad': 
        console.log('In a bad mood... Take a nap 😴');
        break;
    case 'Good' :
        console.log('Write an awesome program! 💻');
        break;
    case 'Bored':
        console.log('Watch a movie 🎬');
        break;
    case 'Energetic' :
        console.log('Go for a run 🏃‍♀️');
        break;
    default:
        console.log('Sit around 🪑');
        break;
}