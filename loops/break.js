// Break can be used to terminate a loop

let counter = 0;
while (counter < 10) {
    console.log('Loop number ' + counter + ' started.');
    if (counter === 5) {
        console.log('Cancelling loop');
        break;
    }

    counter++;
    console.log('Loop number ' + counter + ' ended.');
}

console.log('Loop was finsished.');