// While loop is called a "Pretest" loop
// A condition must be met before loop will begin

let needSleep = true;
let hoursSlept = 0;

// Check if needSleep is true
while (needSleep) {
    hoursSlept += 1;
    console.log('Hours slept: ' + hoursSlept);
    needSleep = hoursSlept <= 8;
    console.log('Still needs sleep? ' + needSleep);
}