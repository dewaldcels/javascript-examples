function Controllable() {
    this.direction = 'forward';
    this.isMoving = false;
}

Controllable.prototype.move = function() {
    this.isMoving = true;
}

function Player() {
    this.health = 100;
    this.inventory = [];
}

// Let the player inherit the prototype from the Controllable class.
Player.prototype = Object.create(Controllable.prototype);

const player = new Player();
player.move();
console.log(player);