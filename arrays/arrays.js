newSection('Declaring an array');
// Declare a new array.
const topics = ['AWS', 'JavaScript', 'CSS', 'HTML', 'Cloud Computing', 'NodeJS'];

console.log(topics);

// Add a new topic
topics.push('Express');

// Add another topic
topics.push('Objective-C');

// Remove last topic added
topics.pop();

newSection('Access an array');

// Accessing the second element of the array.
const favouriteTopic = topics[1];
console.log('My favourite topic is: ' + favouriteTopic);

newSection('Get number of elements');

// Get the number of topics available
console.log('Number of elements in topcis array: ' + topics.length);


newSection('.splice()');

/**
 * .splice() - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/splice
 * Use the .splice() to remove Cloud Computing.
 * 4 - the index 
 * 1 - the number of items to remove
 */

const removedItem = topics.splice(4, 1);

//Removed Item returned by the .splice() function
console.log(removedItem); // => [ "Cloud Computing" ]

// Remaining Topics
console.log(topics); // => [ "AWS", "JavaScript", "CSS", "HTML", "Cloud Computing", "Express" ]

newSection('.reverse()');

/**
 * .reverse()
 * Reverse an array.
 */
topics.reverse();
console.log('Topics reversed: ', topics);









/// IGNORE ME - HELPER FUNCTION
function newSection(title) {
    console.log('\n-----------------------------------------------------------------------------');
    console.log(title);
    console.log('-----------------------------------------------------------------------------\n');
}