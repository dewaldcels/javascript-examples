// IIFE - Immediatly invoked function expression
// A function that immediatly executes itself - No need to invoke it
// Used to protect values.
(function() {
   console.log('Im automatically executed! Yay!'); 
   // Write some awesome code in here.
})();

// Use cases for IIFE - 
// https://mariusschulz.com/blog/use-cases-for-javascripts-iifes