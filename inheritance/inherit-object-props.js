// Base constructor
function Character(baseHealth, isAlive) {
    this.health = baseHealth;
    this.alive = isAlive;
}

function Enemy() {
    // Share the properties of the Character
    // Provide context and baseHealth
    Character.call(this, 100, true);
    this.type = 'Small fry';
}

function Player() {
    // Share the properties of the Character
    Character.call(this, 100);
    this.inventory = [];
}

const player = new Player();
const smallEnemy = new Enemy();

console.log(player);
console.log(smallEnemy);