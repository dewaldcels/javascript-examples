// Closures help us build reusable functions with similar functionality but slight variations.
const multiplier = function(number1) {
    // Return a function.. inside a function.
    return function(number2) {
        // number1 is stored in this execution context
        return number1 * number2;
    }
}

// Build a funtion that can multiply by 2
const timesTwo = multiplier(2);
const ten      = timesTwo(5);

// Build a function that can multiply by 5
const timesFive = multiplier(5);
const twenty = timesFive(4);