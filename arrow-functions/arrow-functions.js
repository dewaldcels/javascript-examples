const square = (n) => {
    return n * n;
}

const twoSquared = square(2);
const threeSquared = square(3);

// No need for { }'s - expression (n * 2) is automatically return without { }'s
const timesTwo = (n) => n * 2; 

const ten = timesTwo(5);
const twenty = timesTwo(10);

// IF! There is only 1 argument - You can omit the ( )  as well
const timesThree = n => n * 3;
const nine = timesThree(3);

const test = () =>{
    console.log('TEstong!');
}

test();