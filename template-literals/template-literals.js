const name = 'Dewald';
const bio = `Hello ${name}, welcome to the future!`;
const imgSrc = "users/dewald.png";
const template = `
    <div class="profile">
        <h4>${name}'s Profile</h4>
        <p>${bio}</p>
        <img src="${imgSrc}" alt="Dewald Avatar" />
    </div>
`
elProfile.append(template);